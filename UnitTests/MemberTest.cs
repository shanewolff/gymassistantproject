﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GymAssistantProject.Model;


namespace UnitTests
{
    [TestClass]
    public class MemberTest
    {
        [TestMethod]
        public void test_setName()
        {
            Member member = new Member();
            bool actual = member.setName("Shane");
            bool expected = true;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void test_setHeight()
        {
            Member member = new Member();

            bool actual = member.setHeight("150");
            bool expected = true;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void test_setNIC()
        {
            Member member = new Member();

            bool actual = member.setNIC("150");
            bool expected = false;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void test_setAddress()
        {
            Member member = new Member();

            bool actual = member.setAddress("");
            bool expected = false;
            Assert.AreEqual(expected, actual);
        }




    }
}
