﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using GymAssistantProject.Model;

namespace UnitTests
{
    [TestClass]
    public class UserTest
    {
        [TestMethod]
        public void test_setPassword()
        {
            Users users = new Users();
            string message;
            bool actual = users.setPassword("admin@123", out message);
            bool expected = true;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void test_setType()
        {
            Users users = new Users();
            users.setType("Admin");
            string actual = users.getType();
            string expected = "Admin";
            Assert.AreEqual(expected, actual);
        }
    }
}
