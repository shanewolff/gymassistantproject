﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GymAssistantProject.Model;

namespace GymAssistantProject.View
{
    public partial class LoginForm : Form
    {
        string type;
        public LoginForm()
        {
            InitializeComponent();
        }

        private void loginBtn_Click(object sender, EventArgs e)
        {
            Users user = new Users();
            string message;
            if(user.authenticate(username.Text, password.Text, out type))
            {
                if(type.Equals("Admin"))
                {
                    username.Text = "";
                    password.Text = "";
                    new AdminForm().Show();
                }

                else if(type.Equals("Trainer"))
                {
                    username.Text = "";
                    password.Text = "";
                    new TrainerForm().Show();
                }

                else
                {
                    username.Text = "";
                    password.Text = "";
                    new CashierForm().Show();
                }

                this.Hide();
            }
            else
            { 
                username.Text = "";
                password.Text = "";
                message = "Invalid credentials";
                MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
