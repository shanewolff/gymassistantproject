﻿using System;

namespace GymAssistantProject.View
{
    partial class MeasurementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.clearBtn = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.rightCalf = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.leftCalf = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.rightThigh = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.leftThigh = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.rightForearm = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.leftForearm = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.rightArm = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.leftArm = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.hip = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.waist = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.chest = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.fat = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.bmi = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.height = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.weight = new System.Windows.Forms.TextBox();
            this.deleteBtn = new System.Windows.Forms.Button();
            this.updateBtn = new System.Windows.Forms.Button();
            this.searchIDBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.measurementID = new System.Windows.Forms.TextBox();
            this.addBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.memberID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.date = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.measurementInfoDGV = new System.Windows.Forms.DataGridView();
            this.searchMemberIDBtn = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.searchID = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.comparisonChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label24 = new System.Windows.Forms.Label();
            this.measurementID2 = new System.Windows.Forms.TextBox();
            this.compareBtn = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.measurementID1 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measurementInfoDGV)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comparisonChart)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(224, 41);
            this.label3.TabIndex = 120;
            this.label3.Text = "Measurements";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.clearBtn);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.rightCalf);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.leftCalf);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.rightThigh);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.leftThigh);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.rightForearm);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.leftForearm);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.rightArm);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.leftArm);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.hip);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.waist);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.chest);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.fat);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.bmi);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.height);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.weight);
            this.groupBox1.Controls.Add(this.deleteBtn);
            this.groupBox1.Controls.Add(this.updateBtn);
            this.groupBox1.Controls.Add(this.searchIDBtn);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.measurementID);
            this.groupBox1.Controls.Add(this.addBtn);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.memberID);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.date);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(19, 64);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(952, 608);
            this.groupBox1.TabIndex = 127;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add New Measurement";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(19, 509);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(705, 20);
            this.label21.TabIndex = 162;
            this.label21.Text = "Weight and height should be a three digit number and all other measurements shoul" +
    "d be 2 digit numbers";
            // 
            // clearBtn
            // 
            this.clearBtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearBtn.Location = new System.Drawing.Point(443, 548);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(106, 35);
            this.clearBtn.TabIndex = 161;
            this.clearBtn.Text = "Clear";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.clearBtn_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(660, 446);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(127, 25);
            this.label19.TabIndex = 159;
            this.label19.Text = "Right Calf (in)";
            // 
            // rightCalf
            // 
            this.rightCalf.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rightCalf.Location = new System.Drawing.Point(836, 446);
            this.rightCalf.Name = "rightCalf";
            this.rightCalf.Size = new System.Drawing.Size(90, 31);
            this.rightCalf.TabIndex = 160;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(335, 446);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(114, 25);
            this.label18.TabIndex = 157;
            this.label18.Text = "Left Calf (in)";
            // 
            // leftCalf
            // 
            this.leftCalf.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leftCalf.Location = new System.Drawing.Point(511, 446);
            this.leftCalf.Name = "leftCalf";
            this.leftCalf.Size = new System.Drawing.Size(90, 31);
            this.leftCalf.TabIndex = 158;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(18, 446);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(142, 25);
            this.label17.TabIndex = 155;
            this.label17.Text = "Right Thigh (in)";
            // 
            // rightThigh
            // 
            this.rightThigh.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rightThigh.Location = new System.Drawing.Point(194, 446);
            this.rightThigh.Name = "rightThigh";
            this.rightThigh.Size = new System.Drawing.Size(90, 31);
            this.rightThigh.TabIndex = 156;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(660, 384);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(129, 25);
            this.label16.TabIndex = 153;
            this.label16.Text = "Left Thigh (in)";
            // 
            // leftThigh
            // 
            this.leftThigh.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leftThigh.Location = new System.Drawing.Point(836, 384);
            this.leftThigh.Name = "leftThigh";
            this.leftThigh.Size = new System.Drawing.Size(90, 31);
            this.leftThigh.TabIndex = 154;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(335, 384);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(164, 25);
            this.label15.TabIndex = 151;
            this.label15.Text = "Right Forearm (in)";
            // 
            // rightForearm
            // 
            this.rightForearm.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rightForearm.Location = new System.Drawing.Point(511, 384);
            this.rightForearm.Name = "rightForearm";
            this.rightForearm.Size = new System.Drawing.Size(90, 31);
            this.rightForearm.TabIndex = 152;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(18, 384);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(151, 25);
            this.label14.TabIndex = 149;
            this.label14.Text = "Left Forearm (in)";
            // 
            // leftForearm
            // 
            this.leftForearm.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leftForearm.Location = new System.Drawing.Point(194, 384);
            this.leftForearm.Name = "leftForearm";
            this.leftForearm.Size = new System.Drawing.Size(90, 31);
            this.leftForearm.TabIndex = 150;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(660, 326);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(129, 25);
            this.label13.TabIndex = 147;
            this.label13.Text = "Right Arm (in)";
            // 
            // rightArm
            // 
            this.rightArm.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rightArm.Location = new System.Drawing.Point(836, 326);
            this.rightArm.Name = "rightArm";
            this.rightArm.Size = new System.Drawing.Size(90, 31);
            this.rightArm.TabIndex = 148;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(335, 326);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(116, 25);
            this.label12.TabIndex = 145;
            this.label12.Text = "Left Arm (in)";
            // 
            // leftArm
            // 
            this.leftArm.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.leftArm.Location = new System.Drawing.Point(511, 326);
            this.leftArm.Name = "leftArm";
            this.leftArm.Size = new System.Drawing.Size(90, 31);
            this.leftArm.TabIndex = 146;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(18, 326);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 25);
            this.label11.TabIndex = 143;
            this.label11.Text = "Hip (in)";
            // 
            // hip
            // 
            this.hip.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hip.Location = new System.Drawing.Point(194, 326);
            this.hip.Name = "hip";
            this.hip.Size = new System.Drawing.Size(90, 31);
            this.hip.TabIndex = 144;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(660, 269);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 25);
            this.label10.TabIndex = 141;
            this.label10.Text = "Waist (in)";
            // 
            // waist
            // 
            this.waist.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.waist.Location = new System.Drawing.Point(836, 269);
            this.waist.Name = "waist";
            this.waist.Size = new System.Drawing.Size(90, 31);
            this.waist.TabIndex = 142;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(335, 269);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 25);
            this.label9.TabIndex = 139;
            this.label9.Text = "Chest (in)";
            // 
            // chest
            // 
            this.chest.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chest.Location = new System.Drawing.Point(511, 269);
            this.chest.Name = "chest";
            this.chest.Size = new System.Drawing.Size(90, 31);
            this.chest.TabIndex = 140;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(18, 269);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 25);
            this.label8.TabIndex = 137;
            this.label8.Text = "Fat (%)";
            // 
            // fat
            // 
            this.fat.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fat.Location = new System.Drawing.Point(194, 269);
            this.fat.Name = "fat";
            this.fat.Size = new System.Drawing.Size(90, 31);
            this.fat.TabIndex = 138;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(660, 207);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 25);
            this.label6.TabIndex = 135;
            this.label6.Text = "BMI";
            // 
            // bmi
            // 
            this.bmi.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bmi.Location = new System.Drawing.Point(836, 207);
            this.bmi.Name = "bmi";
            this.bmi.Size = new System.Drawing.Size(90, 31);
            this.bmi.TabIndex = 136;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(335, 207);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 25);
            this.label5.TabIndex = 133;
            this.label5.Text = "Height (cm)";
            // 
            // height
            // 
            this.height.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.height.Location = new System.Drawing.Point(511, 207);
            this.height.Name = "height";
            this.height.Size = new System.Drawing.Size(90, 31);
            this.height.TabIndex = 134;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(18, 207);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 25);
            this.label4.TabIndex = 131;
            this.label4.Text = "Weight (kg)";
            // 
            // weight
            // 
            this.weight.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weight.Location = new System.Drawing.Point(194, 207);
            this.weight.Name = "weight";
            this.weight.Size = new System.Drawing.Size(90, 31);
            this.weight.TabIndex = 132;
            // 
            // deleteBtn
            // 
            this.deleteBtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteBtn.Location = new System.Drawing.Point(568, 548);
            this.deleteBtn.Name = "deleteBtn";
            this.deleteBtn.Size = new System.Drawing.Size(106, 35);
            this.deleteBtn.TabIndex = 130;
            this.deleteBtn.Text = "Delete";
            this.deleteBtn.UseVisualStyleBackColor = true;
            this.deleteBtn.Click += new System.EventHandler(this.deleteBtn_Click);
            // 
            // updateBtn
            // 
            this.updateBtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateBtn.Location = new System.Drawing.Point(694, 548);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Size = new System.Drawing.Size(106, 35);
            this.updateBtn.TabIndex = 129;
            this.updateBtn.Text = "Update";
            this.updateBtn.UseVisualStyleBackColor = true;
            this.updateBtn.Click += new System.EventHandler(this.updateBtn_Click);
            // 
            // searchIDBtn
            // 
            this.searchIDBtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchIDBtn.Location = new System.Drawing.Point(599, 46);
            this.searchIDBtn.Name = "searchIDBtn";
            this.searchIDBtn.Size = new System.Drawing.Size(106, 32);
            this.searchIDBtn.TabIndex = 128;
            this.searchIDBtn.Text = "Search";
            this.searchIDBtn.UseVisualStyleBackColor = true;
            this.searchIDBtn.Click += new System.EventHandler(this.searchIDBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 25);
            this.label1.TabIndex = 127;
            this.label1.Text = "Measurement ID";
            // 
            // measurementID
            // 
            this.measurementID.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.measurementID.Location = new System.Drawing.Point(194, 46);
            this.measurementID.Name = "measurementID";
            this.measurementID.Size = new System.Drawing.Size(375, 31);
            this.measurementID.TabIndex = 126;
            // 
            // addBtn
            // 
            this.addBtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addBtn.Location = new System.Drawing.Point(821, 548);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(106, 35);
            this.addBtn.TabIndex = 125;
            this.addBtn.Text = "Add";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 25);
            this.label2.TabIndex = 108;
            this.label2.Text = "Member ID";
            // 
            // memberID
            // 
            this.memberID.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memberID.Location = new System.Drawing.Point(194, 98);
            this.memberID.Name = "memberID";
            this.memberID.Size = new System.Drawing.Size(375, 31);
            this.memberID.TabIndex = 112;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 149);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 25);
            this.label7.TabIndex = 114;
            this.label7.Text = "Date";
            // 
            // date
            // 
            this.date.CalendarFont = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.date.Location = new System.Drawing.Point(194, 149);
            this.date.MaxDate = DateTime.Today;
            this.date.Name = "date";
            this.date.Size = new System.Drawing.Size(375, 30);
            this.date.TabIndex = 115;
            this.date.Value = DateTime.Today;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.measurementInfoDGV);
            this.groupBox2.Controls.Add(this.searchMemberIDBtn);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.searchID);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(19, 684);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1800, 349);
            this.groupBox2.TabIndex = 128;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Previous Measurements";
            // 
            // measurementInfoDGV
            // 
            this.measurementInfoDGV.AllowUserToAddRows = false;
            this.measurementInfoDGV.AllowUserToDeleteRows = false;
            this.measurementInfoDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.measurementInfoDGV.Location = new System.Drawing.Point(23, 92);
            this.measurementInfoDGV.Name = "measurementInfoDGV";
            this.measurementInfoDGV.ReadOnly = true;
            this.measurementInfoDGV.RowTemplate.Height = 24;
            this.measurementInfoDGV.Size = new System.Drawing.Size(1752, 237);
            this.measurementInfoDGV.TabIndex = 127;
            // 
            // searchMemberIDBtn
            // 
            this.searchMemberIDBtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchMemberIDBtn.Location = new System.Drawing.Point(463, 40);
            this.searchMemberIDBtn.Name = "searchMemberIDBtn";
            this.searchMemberIDBtn.Size = new System.Drawing.Size(106, 32);
            this.searchMemberIDBtn.TabIndex = 125;
            this.searchMemberIDBtn.Text = "Search";
            this.searchMemberIDBtn.UseVisualStyleBackColor = true;
            this.searchMemberIDBtn.Click += new System.EventHandler(this.searchMemberIDBtn_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(18, 41);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(106, 25);
            this.label20.TabIndex = 108;
            this.label20.Text = "Member ID";
            // 
            // searchID
            // 
            this.searchID.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchID.Location = new System.Drawing.Point(146, 40);
            this.searchID.Name = "searchID";
            this.searchID.Size = new System.Drawing.Size(296, 31);
            this.searchID.TabIndex = 112;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label22);
            this.groupBox4.Controls.Add(this.comparisonChart);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.measurementID2);
            this.groupBox4.Controls.Add(this.compareBtn);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.measurementID1);
            this.groupBox4.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(1003, 64);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(816, 608);
            this.groupBox4.TabIndex = 129;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Measurement Comparison";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(18, 149);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(571, 50);
            this.label22.TabIndex = 129;
            this.label22.Text = "Following chart depicts the percentage measurement difference of\r\nmeasurement set" +
    " 2 based on measurement set 1 ";
            // 
            // comparisonChart
            // 
            chartArea1.Name = "ChartArea1";
            this.comparisonChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.comparisonChart.Legends.Add(legend1);
            this.comparisonChart.Location = new System.Drawing.Point(23, 222);
            this.comparisonChart.Name = "comparisonChart";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.Font = new System.Drawing.Font("Segoe UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series1.IsValueShownAsLabel = true;
            series1.Legend = "Legend1";
            series1.Name = "Percentage Differences";
            this.comparisonChart.Series.Add(series1);
            this.comparisonChart.Size = new System.Drawing.Size(768, 369);
            this.comparisonChart.TabIndex = 128;
            this.comparisonChart.Text = "chart1";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(18, 90);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(166, 25);
            this.label24.TabIndex = 126;
            this.label24.Text = "Measurement ID 2";
            // 
            // measurementID2
            // 
            this.measurementID2.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.measurementID2.Location = new System.Drawing.Point(245, 87);
            this.measurementID2.Name = "measurementID2";
            this.measurementID2.Size = new System.Drawing.Size(146, 31);
            this.measurementID2.TabIndex = 127;
            // 
            // compareBtn
            // 
            this.compareBtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.compareBtn.Location = new System.Drawing.Point(446, 87);
            this.compareBtn.Name = "compareBtn";
            this.compareBtn.Size = new System.Drawing.Size(106, 35);
            this.compareBtn.TabIndex = 125;
            this.compareBtn.Text = "Compare";
            this.compareBtn.UseVisualStyleBackColor = true;
            this.compareBtn.Click += new System.EventHandler(this.compareBtn_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(18, 41);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(166, 25);
            this.label23.TabIndex = 108;
            this.label23.Text = "Measurement ID 1";
            // 
            // measurementID1
            // 
            this.measurementID1.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.measurementID1.Location = new System.Drawing.Point(245, 38);
            this.measurementID1.Name = "measurementID1";
            this.measurementID1.Size = new System.Drawing.Size(146, 31);
            this.measurementID1.TabIndex = 112;
            // 
            // MeasurementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1836, 1045);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Name = "MeasurementForm";
            this.Text = "MeasurementForm";
            this.Load += new System.EventHandler(this.MeasurementForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.measurementInfoDGV)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comparisonChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button deleteBtn;
        private System.Windows.Forms.Button updateBtn;
        private System.Windows.Forms.Button searchIDBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox measurementID;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox memberID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker date;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox height;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox weight;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox bmi;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox rightCalf;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox leftCalf;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox rightThigh;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox leftThigh;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox rightForearm;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox leftForearm;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox rightArm;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox leftArm;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox hip;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox waist;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox chest;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox fat;
        private System.Windows.Forms.Button clearBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView measurementInfoDGV;
        private System.Windows.Forms.Button searchMemberIDBtn;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox searchID;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataVisualization.Charting.Chart comparisonChart;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox measurementID2;
        private System.Windows.Forms.Button compareBtn;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox measurementID1;
        private System.Windows.Forms.Label label22;
    }
}