﻿using System;

namespace GymAssistantProject.View
{
    partial class AttendanceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.finishTime = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.startTime = new System.Windows.Forms.DateTimePicker();
            this.addBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.id = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.date = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.memberAttendanceDGV = new System.Windows.Forms.DataGridView();
            this.searchMemberAttendanceBtn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.searchID = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.searchAttendanceDTP = new System.Windows.Forms.DateTimePicker();
            this.dailyAttendanceDGV = new System.Windows.Forms.DataGridView();
            this.searchDailyAttendanceBtn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memberAttendanceDGV)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dailyAttendanceDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(24, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(179, 41);
            this.label3.TabIndex = 120;
            this.label3.Text = "Attendance";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.finishTime);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.startTime);
            this.groupBox1.Controls.Add(this.addBtn);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.id);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.date);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(811, 81);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(588, 321);
            this.groupBox1.TabIndex = 127;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add Attendance Entry";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 196);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 25);
            this.label4.TabIndex = 128;
            this.label4.Text = "Finish Time";
            // 
            // finishTime
            // 
            this.finishTime.CalendarFont = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.finishTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.finishTime.Location = new System.Drawing.Point(191, 192);
            this.finishTime.MaxDate = new System.DateTime(2017, 4, 30, 0, 0, 0, 0);
            this.finishTime.Name = "finishTime";
            this.finishTime.ShowUpDown = true;
            this.finishTime.Size = new System.Drawing.Size(375, 30);
            this.finishTime.TabIndex = 129;
            this.finishTime.Value = new System.DateTime(2017, 4, 29, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(15, 149);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 25);
            this.label1.TabIndex = 126;
            this.label1.Text = "Start Time";
            // 
            // startTime
            // 
            this.startTime.CalendarFont = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.startTime.Location = new System.Drawing.Point(191, 145);
            this.startTime.MaxDate = new System.DateTime(2017, 4, 30, 0, 0, 0, 0);
            this.startTime.Name = "startTime";
            this.startTime.ShowUpDown = true;
            this.startTime.Size = new System.Drawing.Size(375, 30);
            this.startTime.TabIndex = 127;
            this.startTime.Value = new System.DateTime(2017, 4, 29, 0, 0, 0, 0);
            // 
            // addBtn
            // 
            this.addBtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addBtn.Location = new System.Drawing.Point(460, 245);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(106, 34);
            this.addBtn.TabIndex = 125;
            this.addBtn.Text = "Add";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 25);
            this.label2.TabIndex = 108;
            this.label2.Text = "Member ID";
            // 
            // id
            // 
            this.id.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.id.Location = new System.Drawing.Point(191, 51);
            this.id.Name = "id";
            this.id.Size = new System.Drawing.Size(375, 31);
            this.id.TabIndex = 112;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(15, 99);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 25);
            this.label7.TabIndex = 114;
            this.label7.Text = "Date";
            // 
            // date
            // 
            this.date.CalendarFont = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.date.Location = new System.Drawing.Point(191, 99);
            this.date.MaxDate = DateTime.Today;
            this.date.Name = "date";
            this.date.Size = new System.Drawing.Size(375, 30);
            this.date.TabIndex = 115;
            this.date.Value = DateTime.Today;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.memberAttendanceDGV);
            this.groupBox2.Controls.Add(this.searchMemberAttendanceBtn);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.searchID);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(31, 408);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(759, 325);
            this.groupBox2.TabIndex = 128;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Check Attendance of a Member";
            // 
            // memberAttendanceDGV
            // 
            this.memberAttendanceDGV.AllowUserToAddRows = false;
            this.memberAttendanceDGV.AllowUserToDeleteRows = false;
            this.memberAttendanceDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.memberAttendanceDGV.Location = new System.Drawing.Point(23, 97);
            this.memberAttendanceDGV.Name = "memberAttendanceDGV";
            this.memberAttendanceDGV.ReadOnly = true;
            this.memberAttendanceDGV.RowTemplate.Height = 24;
            this.memberAttendanceDGV.Size = new System.Drawing.Size(713, 191);
            this.memberAttendanceDGV.TabIndex = 126;
            // 
            // searchMemberAttendanceBtn
            // 
            this.searchMemberAttendanceBtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchMemberAttendanceBtn.Location = new System.Drawing.Point(336, 40);
            this.searchMemberAttendanceBtn.Name = "searchMemberAttendanceBtn";
            this.searchMemberAttendanceBtn.Size = new System.Drawing.Size(106, 33);
            this.searchMemberAttendanceBtn.TabIndex = 125;
            this.searchMemberAttendanceBtn.Text = "Search";
            this.searchMemberAttendanceBtn.UseVisualStyleBackColor = true;
            this.searchMemberAttendanceBtn.Click += new System.EventHandler(this.searchMemberAttendanceBtn_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 25);
            this.label5.TabIndex = 108;
            this.label5.Text = "Member ID";
            // 
            // searchID
            // 
            this.searchID.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchID.Location = new System.Drawing.Point(130, 40);
            this.searchID.Name = "searchID";
            this.searchID.Size = new System.Drawing.Size(188, 31);
            this.searchID.TabIndex = 112;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.searchAttendanceDTP);
            this.groupBox3.Controls.Add(this.dailyAttendanceDGV);
            this.groupBox3.Controls.Add(this.searchDailyAttendanceBtn);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(31, 81);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(759, 321);
            this.groupBox3.TabIndex = 129;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Check Daily Attendance";
            // 
            // searchAttendanceDTP
            // 
            this.searchAttendanceDTP.CalendarFont = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchAttendanceDTP.Location = new System.Drawing.Point(75, 42);
            this.searchAttendanceDTP.MaxDate = DateTime.Today;
            this.searchAttendanceDTP.Name = "searchAttendanceDTP";
            this.searchAttendanceDTP.Size = new System.Drawing.Size(375, 30);
            this.searchAttendanceDTP.TabIndex = 127;
            this.searchAttendanceDTP.Value = DateTime.Today;
            // 
            // dailyAttendanceDGV
            // 
            this.dailyAttendanceDGV.AllowUserToAddRows = false;
            this.dailyAttendanceDGV.AllowUserToDeleteRows = false;
            this.dailyAttendanceDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dailyAttendanceDGV.Location = new System.Drawing.Point(23, 93);
            this.dailyAttendanceDGV.Name = "dailyAttendanceDGV";
            this.dailyAttendanceDGV.ReadOnly = true;
            this.dailyAttendanceDGV.RowTemplate.Height = 24;
            this.dailyAttendanceDGV.Size = new System.Drawing.Size(713, 191);
            this.dailyAttendanceDGV.TabIndex = 126;
            // 
            // searchDailyAttendanceBtn
            // 
            this.searchDailyAttendanceBtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchDailyAttendanceBtn.Location = new System.Drawing.Point(479, 44);
            this.searchDailyAttendanceBtn.Name = "searchDailyAttendanceBtn";
            this.searchDailyAttendanceBtn.Size = new System.Drawing.Size(106, 31);
            this.searchDailyAttendanceBtn.TabIndex = 125;
            this.searchDailyAttendanceBtn.Text = "Search";
            this.searchDailyAttendanceBtn.UseVisualStyleBackColor = true;
            this.searchDailyAttendanceBtn.Click += new System.EventHandler(this.searchDailyAttendanceBtn_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(18, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 25);
            this.label6.TabIndex = 108;
            this.label6.Text = "Date";
            // 
            // AttendanceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1427, 759);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Name = "AttendanceForm";
            this.Text = "AttendanceForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memberAttendanceDGV)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dailyAttendanceDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox id;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker date;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker finishTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker startTime;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView memberAttendanceDGV;
        private System.Windows.Forms.Button searchMemberAttendanceBtn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox searchID;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dailyAttendanceDGV;
        private System.Windows.Forms.Button searchDailyAttendanceBtn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker searchAttendanceDTP;
    }
}