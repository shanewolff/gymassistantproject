﻿namespace GymAssistantProject.View
{
    partial class TrainerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exerciseBtn = new System.Windows.Forms.Button();
            this.measurementBtn = new System.Windows.Forms.Button();
            this.subscriptionBtn = new System.Windows.Forms.Button();
            this.attendanceBtn = new System.Windows.Forms.Button();
            this.memberBtn = new System.Windows.Forms.Button();
            this.signOutBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // exerciseBtn
            // 
            this.exerciseBtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exerciseBtn.Location = new System.Drawing.Point(203, 239);
            this.exerciseBtn.Name = "exerciseBtn";
            this.exerciseBtn.Size = new System.Drawing.Size(144, 58);
            this.exerciseBtn.TabIndex = 150;
            this.exerciseBtn.Text = "Exercises";
            this.exerciseBtn.UseVisualStyleBackColor = true;
            this.exerciseBtn.Click += new System.EventHandler(this.exerciseBtn_Click);
            // 
            // measurementBtn
            // 
            this.measurementBtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.measurementBtn.Location = new System.Drawing.Point(22, 239);
            this.measurementBtn.Name = "measurementBtn";
            this.measurementBtn.Size = new System.Drawing.Size(144, 58);
            this.measurementBtn.TabIndex = 149;
            this.measurementBtn.Text = "Measurements";
            this.measurementBtn.UseVisualStyleBackColor = true;
            this.measurementBtn.Click += new System.EventHandler(this.measurementBtn_Click);
            // 
            // subscriptionBtn
            // 
            this.subscriptionBtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subscriptionBtn.Location = new System.Drawing.Point(203, 137);
            this.subscriptionBtn.Name = "subscriptionBtn";
            this.subscriptionBtn.Size = new System.Drawing.Size(144, 58);
            this.subscriptionBtn.TabIndex = 148;
            this.subscriptionBtn.Text = "Subscriptions";
            this.subscriptionBtn.UseVisualStyleBackColor = true;
            this.subscriptionBtn.Click += new System.EventHandler(this.subscriptionBtn_Click);
            // 
            // attendanceBtn
            // 
            this.attendanceBtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attendanceBtn.Location = new System.Drawing.Point(386, 137);
            this.attendanceBtn.Name = "attendanceBtn";
            this.attendanceBtn.Size = new System.Drawing.Size(144, 58);
            this.attendanceBtn.TabIndex = 146;
            this.attendanceBtn.Text = "Attendance";
            this.attendanceBtn.UseVisualStyleBackColor = true;
            this.attendanceBtn.Click += new System.EventHandler(this.attendanceBtn_Click);
            // 
            // memberBtn
            // 
            this.memberBtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.memberBtn.Location = new System.Drawing.Point(22, 137);
            this.memberBtn.Name = "memberBtn";
            this.memberBtn.Size = new System.Drawing.Size(144, 58);
            this.memberBtn.TabIndex = 145;
            this.memberBtn.Text = "Memebers";
            this.memberBtn.UseVisualStyleBackColor = true;
            this.memberBtn.Click += new System.EventHandler(this.memberBtn_Click);
            // 
            // signOutBtn
            // 
            this.signOutBtn.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.signOutBtn.Location = new System.Drawing.Point(416, 349);
            this.signOutBtn.Name = "signOutBtn";
            this.signOutBtn.Size = new System.Drawing.Size(106, 35);
            this.signOutBtn.TabIndex = 144;
            this.signOutBtn.Text = "Sign Out";
            this.signOutBtn.UseVisualStyleBackColor = true;
            this.signOutBtn.Click += new System.EventHandler(this.signOutBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(495, 25);
            this.label2.TabIndex = 143;
            this.label2.Text = "You are signed in as a trainer. Chose a service to proceed.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(178, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(209, 41);
            this.label3.TabIndex = 142;
            this.label3.Text = "GymAssistant";
            // 
            // TrainerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 405);
            this.ControlBox = false;
            this.Controls.Add(this.exerciseBtn);
            this.Controls.Add(this.measurementBtn);
            this.Controls.Add(this.subscriptionBtn);
            this.Controls.Add(this.attendanceBtn);
            this.Controls.Add(this.memberBtn);
            this.Controls.Add(this.signOutBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Name = "TrainerForm";
            this.Text = "TrainerForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button exerciseBtn;
        private System.Windows.Forms.Button measurementBtn;
        private System.Windows.Forms.Button subscriptionBtn;
        private System.Windows.Forms.Button attendanceBtn;
        private System.Windows.Forms.Button memberBtn;
        private System.Windows.Forms.Button signOutBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}