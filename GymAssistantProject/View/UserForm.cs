﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GymAssistantProject.Model;
using System.Data;

namespace GymAssistantProject.View
{
    public partial class UserForm : Form
    {
        Users user;
        public UserForm()
        {
            InitializeComponent();
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            string message;
            Users users = new Users();
            if(users.setUsername(username.Text, out message))
            {
                if(users.setPassword(password.Text, out message))
                {
                    users.setType(type.SelectedItem.ToString());
                    if(users.addUser(out message))
                    {
                        MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    } 
                }
                else
                {
                    MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void refreshBtn_Click(object sender, EventArgs e)
        {
            string message;
            Users user = new Users();
            DataTable info = user.getUsersInfo(out message);
            userInfoDGV.DataSource = info;
            if(info == null)
            {
                MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void UserForm_Load(object sender, EventArgs e)
        {
            this.type.SelectedIndex = 0;
            deleteBtn.Enabled = false;
        }

        private void searchIDBtn_Click(object sender, EventArgs e)
        {
            user = new Users();
            string message;
            if(user.getUser(userID.Text, out message))
            {
                username.Text = user.getUsername();
                password.Text = null;
                type.SelectedItem = user.getType();
                deleteBtn.Enabled = true;
                addBtn.Enabled = false;
            }
            else
            {
                MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void clearFields()
        {
            userID.Text = "";
            username.Text = "";
            password.Text = "";
        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            clearFields();
            deleteBtn.Enabled = false;
            addBtn.Enabled = true;
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            string message;
            if(user.deleteUser(out message))
            {
                deleteBtn.Enabled = false;
                addBtn.Enabled = true;
                clearFields();
                user = null;
                MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
            }

            else
            {
                MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
