﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GymAssistantProject.Model;

namespace GymAssistantProject.View
{
    public partial class SubscriptionForm : Form
    {
        Subscription subscription;
        public SubscriptionForm()
        {
            InitializeComponent();
            
        }

        private void SubscriptionForm_Load(object sender, EventArgs e)
        {
            type.SelectedItem = "Month";
            updateBtn.Enabled = false;
            deleteBtn.Enabled = false;
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            Subscription subscription = new Subscription();

            try
            {
                if (subscription.setMemberID(Convert.ToInt32(id.Text)))
                {
                    subscription.setType(type.Text);
                    subscription.setCommenceDate(commenceDate.Value);
                    subscription.setExpiryDate();
                    if (subscription.addSubscription())
                    {
                        string message = "Subscription is successfully added" + Environment.NewLine + subscription.getDetails();
                        MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    else
                    {
                        string error = "Subscription could not be saved to the database";
                        MessageBox.Show(error, "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                else
                {
                    string error = "Entered id does not exist in the database";
                    MessageBox.Show(error, "Invalid User Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            catch (FormatException)
            {
                string error = "Entered id is not a valid integer";
                MessageBox.Show(error, "Invalid User Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            Subscription subscription = new Subscription();
            DataTable subscriptionInfo;
            try
            {
                subscription.getSubscription(Convert.ToInt32(searchID.Text), out subscriptionInfo);
                if (subscriptionInfo != null)
                {
                    subscriptionInfoDGV.DataSource = subscriptionInfo;
                    
                }

                else 
                {
                    subscriptionInfoDGV.DataSource = subscriptionInfo;
                    string error = "No records exist for the given member id";
                    MessageBox.Show(error, "User Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
            catch (FormatException)
            {
                string error = "Member id entered is not a valid integer";
                MessageBox.Show(error, "User Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (subscription.setMemberID(Convert.ToInt32(id.Text)))
                {
                    subscription.setType(type.Text);
                    subscription.setCommenceDate(commenceDate.Value);
                    subscription.setExpiryDate();
                    if (subscription.updateSubscription())
                    {

                        string message = "Subscription is successfully updated" + Environment.NewLine + subscription.getDetails();
                        MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    else
                    {
                        string error = "Subscription could not be saved to the database";
                        MessageBox.Show(error, "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                else
                {
                    string error = "Entered id does not exist in the database";
                    MessageBox.Show(error, "Invalid User Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            catch (FormatException)
            {
                string error = "Entered id is not a valid integer";
                MessageBox.Show(error, "Invalid User Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void searchIDBtn_Click(object sender, EventArgs e)
        {
            subscription = new Subscription();
            try
            {
                if (subscription.searchSubscriptionByID(Convert.ToInt32(subscriptionID.Text)))
                {
                    id.Text = subscription.getMemberID().ToString();
                    type.SelectedItem = subscription.getType();
                    commenceDate.Value = subscription.getCommenceDate();
                    updateBtn.Enabled = true;
                    deleteBtn.Enabled = true;
                }

                else
                {
                    string error = "Subscription id entered does not exist in the database";
                    MessageBox.Show(error, "User Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            catch (FormatException)
            {
                string error = "Subscription id entered is not a valid integer";
                MessageBox.Show(error, "User Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            if (subscription.deleteSubscription())
            {
                updateBtn.Enabled = false;
                deleteBtn.Enabled = false;
                string message = "Subscription is successfully deleted";
                MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
            else
            {
                string error = "Subscription cannot be deleted";
                MessageBox.Show(error, "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } 
        }

        private void refreshBtn_Click(object sender, EventArgs e)
        {
            Subscription subscription = new Subscription();
            DataTable expiringList = subscription.getExpiringList();
            if (expiringList != null)
            {
                expiringSubscriptionDGV.DataSource = expiringList;
            }

            else
            {
                expiringSubscriptionDGV.DataSource = null;
                string message = "No subscriptions expiring within next 7 days";
                MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
