﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GymAssistantProject.View
{
    public partial class CashierForm : Form
    {
        public CashierForm()
        {
            InitializeComponent();
        }

        private void subscriptionBtn_Click(object sender, EventArgs e)
        {
            new SubscriptionForm().Show();
        }

        private void attendanceBtn_Click(object sender, EventArgs e)
        {
            new AttendanceForm().Show();
        }

        private void signOutBtn_Click(object sender, EventArgs e)
        {
            Program.loginInstance.Show();
            this.Close();
        }
    }
}
