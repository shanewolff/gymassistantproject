﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GymAssistantProject.View
{
    public partial class TrainerForm : Form
    {
        public TrainerForm()
        {
            InitializeComponent();
        }

        private void memberBtn_Click(object sender, EventArgs e)
        {
            new MemberForm().Show();
        }

        private void subscriptionBtn_Click(object sender, EventArgs e)
        {
            new SubscriptionForm().Show();
        }

        private void attendanceBtn_Click(object sender, EventArgs e)
        {
            new AttendanceForm().Show();
        }

        private void measurementBtn_Click(object sender, EventArgs e)
        {
            new MeasurementForm().Show();
        }

        private void exerciseBtn_Click(object sender, EventArgs e)
        {
            new ExerciseForm().Show();
        }

        private void signOutBtn_Click(object sender, EventArgs e)
        {
            Program.loginInstance.Show();
            this.Close();
        }
    }
}
