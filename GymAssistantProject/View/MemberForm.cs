﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GymAssistantProject.Model;

namespace GymAssistantProject.View
{
    public partial class MemberForm : Form
    {
        private Member member;
        public MemberForm()
        {
            InitializeComponent();
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            List<string> errorList = new List<string>();

            Member member = new Member();

            if (!member.setName(name.Text))
            {
                errorList.Add("Incorrect name");
            }

            if (!member.setNIC(nic.Text))
            {
                errorList.Add("Incorrect NIC");
            }

            if (!member.setAddress(address.Text))
            {
                errorList.Add("Address is empty");
            }

            member.setEmail(email.Text);

            if (!member.setHeight(height.Text))
            {
                errorList.Add("Height should be an integer");
            }

            member.setDOB(dob.Value);

            if (!member.setContact(contact.Text))
            {
                errorList.Add("Contact number format is incorrect");
            }

            if (!member.setPrimaryGoal(primaryGoal.Text))
            {
                errorList.Add("Priamry goal is empty");
            }

            member.setFitnessLevel(fitnessLevel.GetItemText(fitnessLevel.SelectedItem));

            member.setDiseases(diseases.Text);

            member.setRegistrationDate(registrationDate.Value);

            if (errorList.Count == 0)
            {
                if (!member.saveMember())
                {
                    string error = "Record cannot be inserted due to database validation error";
                    MessageBox.Show(error, "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                else
                {
                    string message = "Record succesfully entered";
                    MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    clearFields();
                    setSearchAutoCompleteSource();
                }
            }
            else
            {
                string error = "Cannot save the member due to following errors" + Environment.NewLine;
                foreach (string errorline in errorList)
                {
                    error = error + errorline + Environment.NewLine;
                }
                MessageBox.Show(error, "Input Data Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void clearFields()
        {
            id.Text = "";
            name.Text = "";
            address.Text = "";
            height.Text = "";
            contact.Text = "";
            primaryGoal.Text = "";
            nic.Text = "";
            email.Text = "";
            diseases.Text = "";
            saveBtn.Enabled = true;
            savePDFBtn.Enabled = false;
            updateBtn.Enabled = false;
        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            clearFields();
        }

        private void MemberForm_Load(object sender, EventArgs e)
        {
            setSearchAutoCompleteSource();
            searchMethod.SelectedItem = "Name";
            fitnessLevel.SelectedItem = "Beginner";
            savePDFBtn.Enabled = false;
            updateBtn.Enabled = false;
        }

        private void setSearchAutoCompleteSource()
        {
            Member member = new Member();
            DataTable members = new DataTable();
            members = member.getMembers();
            AutoCompleteStringCollection newCollection = new AutoCompleteStringCollection();
            foreach (DataRow row in members.Rows)
            {
                newCollection.Add(Convert.ToString(row[1]));
            }
            nameSearch.AutoCompleteCustomSource = newCollection;
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(searchMethod.SelectedItem).Equals("Name"))
            {
                member = new Member();
                DataTable members = new DataTable();
                members = member.getMemberByName(nameSearch.Text);
                DataRow[] rows = members.Select();
                if (rows.Length != 0)
                {
                    id.Text = rows[0]["memberID"].ToString();
                    name.Text = rows[0]["name"].ToString();
                    nic.Text = rows[0]["nic"].ToString();
                    address.Text = rows[0]["address"].ToString();
                    email.Text = rows[0]["email"].ToString();
                    height.Text = rows[0]["height"].ToString();
                    dob.Value = Convert.ToDateTime(rows[0]["dob"]);
                    contact.Text = rows[0]["contact"].ToString();
                    primaryGoal.Text = rows[0]["primaryGoal"].ToString();
                    if (Convert.ToInt32(rows[0]["fitnessLevel"]) == 1)
                    {
                        fitnessLevel.SelectedItem = "Beginner";
                    }
                    if (Convert.ToInt32(rows[0]["fitnessLevel"]) == 2)
                    {
                        fitnessLevel.SelectedItem = "Some Experience";
                    }
                    if (Convert.ToInt32(rows[0]["fitnessLevel"]) == 3)
                    {
                        fitnessLevel.SelectedItem = "Advanced";
                    }
                    diseases.Text = rows[0]["diseases"].ToString();
                    registrationDate.Value = Convert.ToDateTime(rows[0]["registrationDate"]);
                    saveBtn.Enabled = false;
                    savePDFBtn.Enabled = true;
                    updateBtn.Enabled = true;
                }
                else
                {
                    string error = "Name does not exist in the database";
                    MessageBox.Show(error, "Input Data Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            if (Convert.ToString(searchMethod.SelectedItem).Equals("ID"))
            {
                member = new Member();
                DataTable members = new DataTable();
                try
                {
                    members = member.getMemberByID(Convert.ToInt32(nameSearch.Text));
                    DataRow[] rows = members.Select();
                    if (rows.Length != 0)
                    {
                        id.Text = rows[0]["memberID"].ToString();
                        name.Text = rows[0]["name"].ToString();
                        nic.Text = rows[0]["nic"].ToString();
                        address.Text = rows[0]["address"].ToString();
                        email.Text = rows[0]["email"].ToString();
                        height.Text = rows[0]["height"].ToString();
                        dob.Value = Convert.ToDateTime(rows[0]["dob"]);
                        contact.Text = rows[0]["contact"].ToString();
                        primaryGoal.Text = rows[0]["primaryGoal"].ToString();
                        if (Convert.ToInt32(rows[0]["fitnessLevel"]) == 1)
                        {
                            fitnessLevel.SelectedItem = "Beginner";
                        }
                        if (Convert.ToInt32(rows[0]["fitnessLevel"]) == 2)
                        {
                            fitnessLevel.SelectedItem = "Some Experience";
                        }
                        if (Convert.ToInt32(rows[0]["fitnessLevel"]) == 3)
                        {
                            fitnessLevel.SelectedItem = "Advanced";
                        }
                        diseases.Text = rows[0]["diseases"].ToString();
                        registrationDate.Value = Convert.ToDateTime(rows[0]["registrationDate"]);
                        saveBtn.Enabled = false;
                        savePDFBtn.Enabled = true;
                        updateBtn.Enabled = true;
                    }
                    else
                    {
                        string error = "ID does not exist in the database";
                        MessageBox.Show(error, "Input Data Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (FormatException)
                {
                    string error = "Entered value for id is not a valid integer";
                    MessageBox.Show(error, "Input Data Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
                
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            List<string> errorList = new List<string>();

            Member member = new Member();

            if (!member.setName(name.Text))
            {
                errorList.Add("Incorrect name");
            }

            if (!member.setNIC(nic.Text))
            {
                errorList.Add("Incorrect NIC");
            }

            if (!member.setAddress(address.Text))
            {
                errorList.Add("Address is empty");
            }

            member.setEmail(email.Text);

            if (!member.setHeight(height.Text))
            {
                errorList.Add("Height should be an integer");
            }

            member.setDOB(dob.Value);

            if (!member.setContact(contact.Text))
            {
                errorList.Add("Contact number format is incorrect");
            }

            if (!member.setPrimaryGoal(primaryGoal.Text))
            {
                errorList.Add("Priamry goal is empty");
            }

            member.setFitnessLevel(fitnessLevel.GetItemText(fitnessLevel.SelectedItem));

            member.setDiseases(diseases.Text);

            member.setRegistrationDate(registrationDate.Value);

            if (errorList.Count == 0)
            {
                if (!member.updateMember(Int32.Parse(id.Text)))
                {
                    string error = "Record cannot be updated due to database validation error";
                    MessageBox.Show(error, "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                else
                {
                    string message = "Record succesfully updated";
                    MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    clearFields();
                    setSearchAutoCompleteSource();
                }
            }
            else
            {
                string error = "Cannot save the member due to following errors" + Environment.NewLine;
                foreach (string errorline in errorList)
                {
                    error = error + errorline + Environment.NewLine;
                }
                MessageBox.Show(error, "Input Data Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void savePDFBtn_Click(object sender, EventArgs e)
        {
            saveFileDialog.DefaultExt = ".pdf";
            saveFileDialog.AddExtension = true;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if(member.saveToPDF(saveFileDialog.FileName))
                {
                    MessageBox.Show("PDF document has been successfully saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Could not save the file", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
            }
            else
            {
                MessageBox.Show("No file has been saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
