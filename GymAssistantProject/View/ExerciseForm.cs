﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GymAssistantProject.Model;

namespace GymAssistantProject.View
{
    public partial class ExerciseForm : Form
    {
        Exercise exercise;
        public ExerciseForm()
        {
            InitializeComponent();
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            string message;
            exercise = new Exercise();
            if(exercise.setName(exerciseName.Text, out message))
            {
                if (exercise.setType(exerciseType.Text, out message))
                {
                    if (exercise.addExercise(out message))
                    {
                        MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    else
                    {
                        MessageBox.Show(message, "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                else
                {
                    MessageBox.Show(message, "User Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            else
            {
                MessageBox.Show(message, "User Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void refreshBtn_Click(object sender, EventArgs e)
        {
            Exercise new_exercise = new Exercise();
            DataTable info;
            new_exercise.getExercises(out info);
            if(info != null)
            {
                exerciseInfoDGV.DataSource = info;
            }
            else
            {
                exerciseInfoDGV.DataSource = null;
                string message = "No data exist in the database";
                MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        private void searchIDBtn_Click(object sender, EventArgs e)
        {
            string message;
            int error;
            exercise = new Exercise();
            if(exercise.getExerciseByID(exerciseID.Text, out message, out error))
            {
                exerciseName.Text = exercise.getName();
                exerciseType.Text = exercise.getType();
                deleteBtn.Enabled = true;
                updateBtn.Enabled = true;
                addBtn.Enabled = false;
            }

            else
            {
                if(error == 1)
                {
                    MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                else
                {
                    MessageBox.Show(message, "User Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
            }
        }

        private void clearFields()
        {
            exerciseID.Text = "";
            exerciseName.Text = "";
            exerciseType.Text = "";
        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            clearFields();
            addBtn.Enabled = true;
            deleteBtn.Enabled = false;
            updateBtn.Enabled = false;
        }

        private void ExerciseForm_Load(object sender, EventArgs e)
        {
            deleteBtn.Enabled = false;
            updateBtn.Enabled = false;
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            string message;

            if(exercise.deleteExercise(out message))
            {
                MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                clearFields();
                addBtn.Enabled = true;
                deleteBtn.Enabled = false;
                updateBtn.Enabled = false;
            }
            
            else
            {
                MessageBox.Show(message, "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            string message;

            if (exercise.setName(exerciseName.Text, out message))
            {
                if (exercise.setType(exerciseType.Text, out message))
                {
                    if (exercise.updateExercise(out message))
                    {
                        MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    else
                    {
                        MessageBox.Show(message, "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

                else
                {
                    MessageBox.Show(message, "User Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            else
            {
                MessageBox.Show(message, "User Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
