﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GymAssistantProject.Model;

namespace GymAssistantProject.View
{
    public partial class MeasurementForm : Form
    {
        Measurement measurement;
        List<string> measurements;
        public MeasurementForm()
        {
            InitializeComponent();
        }

        private void addBtn_Click(object sender, EventArgs e)
        {   
            string message;
            measurement = new Measurement();
            if (measurement.setMemberID(memberID.Text, out message))
            {
                measurement.setDate(date.Value);
                measurements = new List<string>();
                measurements.Add(weight.Text);
                measurements.Add(height.Text);
                measurements.Add(bmi.Text);
                measurements.Add(fat.Text);
                measurements.Add(chest.Text);
                measurements.Add(waist.Text);
                measurements.Add(hip.Text);
                measurements.Add(leftArm.Text);
                measurements.Add(rightArm.Text);
                measurements.Add(leftForearm.Text);
                measurements.Add(rightForearm.Text);
                measurements.Add(leftThigh.Text);
                measurements.Add(rightThigh.Text);
                measurements.Add(leftCalf.Text);
                measurements.Add(rightCalf.Text);
                
                if (measurement.setMeasurements(measurements, out message))
                {
                    if (measurement.addNewMeasurement(out message))
                    {
                        MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    else
                    {
                        MessageBox.Show(message, "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    
                    MessageBox.Show(message, "User Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            else
            {
                MessageBox.Show(message, "User Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }    
        }

        private void searchIDBtn_Click(object sender, EventArgs e)
        {
            string message;
            measurement = new Measurement();
            if (measurement.retrieveMeasurementDetails(measurementID.Text, out message))
            {
                memberID.Text = measurement.getMemberID().ToString();
                date.Value = measurement.getDate();
                weight.Text = measurement.getMeasurements()[0].ToString();
                height.Text = measurement.getMeasurements()[1].ToString();
                bmi.Text = measurement.getMeasurements()[2].ToString();
                fat.Text = measurement.getMeasurements()[3].ToString();
                chest.Text = measurement.getMeasurements()[4].ToString();
                waist.Text = measurement.getMeasurements()[5].ToString();
                hip.Text = measurement.getMeasurements()[6].ToString();
                leftArm.Text = measurement.getMeasurements()[7].ToString();
                rightArm.Text = measurement.getMeasurements()[8].ToString();
                leftForearm.Text = measurement.getMeasurements()[9].ToString();
                rightForearm.Text = measurement.getMeasurements()[10].ToString();
                leftThigh.Text = measurement.getMeasurements()[11].ToString();
                rightThigh.Text = measurement.getMeasurements()[12].ToString();
                leftCalf.Text = measurement.getMeasurements()[13].ToString();
                rightCalf.Text = measurement.getMeasurements()[14].ToString();

                updateBtn.Enabled = true;
                deleteBtn.Enabled = true;
            }

            else
            {
                MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void MeasurementForm_Load(object sender, EventArgs e)
        {
            updateBtn.Enabled = false;
            deleteBtn.Enabled = false;
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            string message;
            if (measurement.deleteMeasurement(out message))
            {
                updateBtn.Enabled = false;
                deleteBtn.Enabled = false;
                clearFields();
                MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            else
            {
                MessageBox.Show(message, "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            string message;
            measurement = new Measurement();
            if (measurement.setMemberID(memberID.Text, out message))
            {
                measurement.setDate(date.Value);
                measurements = new List<string>();
                measurements.Add(weight.Text);
                measurements.Add(height.Text);
                measurements.Add(bmi.Text);
                measurements.Add(fat.Text);
                measurements.Add(chest.Text);
                measurements.Add(waist.Text);
                measurements.Add(hip.Text);
                measurements.Add(leftArm.Text);
                measurements.Add(rightArm.Text);
                measurements.Add(leftForearm.Text);
                measurements.Add(rightForearm.Text);
                measurements.Add(leftThigh.Text);
                measurements.Add(rightThigh.Text);
                measurements.Add(leftCalf.Text);
                measurements.Add(rightCalf.Text);

                if (measurement.setMeasurements(measurements, out message))
                {
                    if (measurement.updateMeasurement(out message))
                    {

                        MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    else
                    {
                        MessageBox.Show(message, "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {

                    MessageBox.Show(message, "User Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            else
            {
                MessageBox.Show(message, "User Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            clearFields();
        }

        private void clearFields()
        {
            measurementID.Text = "";
            memberID.Text = "";
            weight.Text = "";
            height.Text = "";
            bmi.Text = "";
            fat.Text = "";
            chest.Text = "";
            waist.Text = "";
            hip.Text = "";
            leftArm.Text = "";
            rightArm.Text = "";
            leftForearm.Text = "";
            rightForearm.Text = "";
            leftThigh.Text = "";
            rightThigh.Text = "";
            leftCalf.Text = "";
            rightCalf.Text = "";
        }

        private void searchMemberIDBtn_Click(object sender, EventArgs e)
        {
            string message;
            Measurement newMeasurement = new Measurement();
            DataTable info = newMeasurement.getPreviousMeasurements(searchID.Text, out message);
            if (info != null)
            {
                measurementInfoDGV.DataSource = info;
            }

            else
            {
                measurementInfoDGV.DataSource = null;
                MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void compareBtn_Click(object sender, EventArgs e)
        {
            foreach (var series in comparisonChart.Series)
            {
                series.Points.Clear();
            }
            List<double> differences = new List<double>();
            string message;
            measurement = new Measurement();
            if(measurement.getPercentDifference(measurementID1.Text, measurementID2.Text, out differences, out message))
            {
                comparisonChart.Series["Percentage Differences"].Points.AddXY("Weight", differences[0]);
                comparisonChart.Series["Percentage Differences"].Points.AddXY("Height", differences[1]);
                comparisonChart.Series["Percentage Differences"].Points.AddXY("BMI", differences[2]);
                comparisonChart.Series["Percentage Differences"].Points.AddXY("Fat", differences[3]);
                comparisonChart.Series["Percentage Differences"].Points.AddXY("Chest", differences[4]);
                comparisonChart.Series["Percentage Differences"].Points.AddXY("Waist", differences[5]);
                comparisonChart.Series["Percentage Differences"].Points.AddXY("Hip", differences[6]);
                comparisonChart.Series["Percentage Differences"].Points.AddXY("Left Arm", differences[7]);
                comparisonChart.Series["Percentage Differences"].Points.AddXY("Right Arm", differences[8]);
                comparisonChart.Series["Percentage Differences"].Points.AddXY("Left Forearm", differences[9]);
                comparisonChart.Series["Percentage Differences"].Points.AddXY("Right Forearm", differences[10]);
                comparisonChart.Series["Percentage Differences"].Points.AddXY("Left Thigh", differences[11]);
                comparisonChart.Series["Percentage Differences"].Points.AddXY("Right Thigh", differences[12]);
                comparisonChart.Series["Percentage Differences"].Points.AddXY("Left Calf", differences[13]);
                comparisonChart.Series["Percentage Differences"].Points.AddXY("Right Calf", differences[14]);
            }

            else
            {
                MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
