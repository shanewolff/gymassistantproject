﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GymAssistantProject.Model;

namespace GymAssistantProject.View
{
    public partial class AttendanceForm : Form
    {
        Attendance attendance;
        public AttendanceForm()
        {
            InitializeComponent();
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            Attendance attendance = new Attendance();

            try
            {
                if (attendance.setMemberID(Convert.ToInt32(id.Text)))
                {
                    attendance.setDate(date.Value);
                    attendance.setStartTime(startTime.Value);
                    attendance.setFinishTime(finishTime.Value);
                    if (attendance.addAttendance())
                    {
                        string message = "Attendance record is successfully added";
                        MessageBox.Show(message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    else
                    {
                        string error = "Attendance could not be saved to the database";
                        MessageBox.Show(error, "Database Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                else
                {
                    string error = "Entered member id does not exist in the database";
                    MessageBox.Show(error, "Invalid User Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            catch (FormatException)
            {
                string error = "Entered member id is not a valid integer";
                MessageBox.Show(error, "Invalid User Input", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void searchMemberAttendanceBtn_Click(object sender, EventArgs e)
        {
            memberAttendanceDGV.DataSource = null;
            Attendance attedance = new Attendance();
            DataTable summary = new DataTable();
            string message = "";

            try
            {
                if (attedance.getAttendanceByMemberID(Convert.ToInt32(searchID.Text), out message, out summary))
                {
                    memberAttendanceDGV.DataSource = summary;
                }

                else
                {
                    MessageBox.Show(message, "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            catch (FormatException)
            {
                string error = "Member id entered is not a valid integer";
                MessageBox.Show(error, "User Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void searchDailyAttendanceBtn_Click(object sender, EventArgs e)
        {
            dailyAttendanceDGV.DataSource = null;
            Attendance attedance = new Attendance();
            DataTable summary = new DataTable();
            string message = "";
            attedance.getAttendanceByDate(searchAttendanceDTP.Value, out message, out summary);
            if (summary != null)
            {
                dailyAttendanceDGV.DataSource = summary;
            }
            else
            {
                MessageBox.Show(message, "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        
    }
}
