﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymAssistantProject.Model
{
    public class Attendance
    {
        private SqlConnection connection;
        private string connectionString = ConfigurationManager.ConnectionStrings["GymAssistantProject.Properties.Settings.GymAssistantDBConnectionString"].ConnectionString;

        private int attendanceID;
        private int memberID;
        private DateTime date;
        private DateTime startTime;
        private DateTime finishTime;

        public bool setMemberID(int id)
        {
            if (isMemberIDValid(id))
            {
                this.memberID = id;
                return true;
            }

            else
            {
                return false;
            }            
        }

        public int getMemberID()
        {
            return this.memberID;
        }


        private bool isMemberIDValid(int id)
        {
            connection = new SqlConnection(connectionString);
            string query = "select count(*) count from Member where Member.memberID = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", id);

            try
            {
                connection.Open();
                int count = (Int32)command.ExecuteScalar();
                connection.Close();
                if (count != 0)
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }

            catch (SqlException)
            {
                return false;
            }
        }

        public void setDate(DateTime date)
        {
            this.date = date;
        }

        public DateTime getDate()
        {
            return this.date;
        }

        public void setStartTime(DateTime startTime)
        {
            this.startTime = startTime;
        }

        public DateTime getStartTime()
        {
            return this.startTime;
        }

        public void setFinishTime(DateTime finishTime)
        {
            this.finishTime = finishTime;
        }

        public DateTime getFinishTime()
        {
            return this.finishTime;
        }

        public bool addAttendance()
        {
            connection = new SqlConnection(connectionString);

            string query = "insert into [Attendance] values (@memberID, @date, @startTime, @finishTime)";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@memberID", this.memberID);
            command.Parameters.AddWithValue("@date", this.date);
            command.Parameters.AddWithValue("@startTime", this.startTime);
            command.Parameters.AddWithValue("@finishTime", this.finishTime);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }

        public bool getAttendanceByMemberID(int id, out string message, out DataTable dataTable)
        {
            if (isMemberIDValid(id))
            {
                string query = "select attendanceID as 'Attendance ID', date as Date, startTime as Start, finishTime as Finish from (select * from Attendance where Attendance.memberID = @id) as Summary order by Summary.date desc;";
                using (connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    command.Parameters.AddWithValue("@id", id);
                    DataTable attendance = new DataTable();
                    adapter.Fill(attendance);

                    if (attendance.Rows.Count != 0)
                    {
                        message = "Data succesfully received";
                        dataTable = attendance;
                        return true;
                    }

                    else
                    {
                        message = "No data found!";
                        dataTable = null;
                        return false;
                    }
                }
            }

            else
            {
                message = "Member ID entered does not exist in the database";
                dataTable = null;
                return false;
            }
        }

        public void getAttendanceByDate(DateTime date, out string message, out DataTable dataTable)
        {
            string query = "select * from (select Attendance.attendanceID as 'Attendance ID', Member.memberID as 'Member ID', Member.name as Name, Attendance.startTime as StartTime, Attendance.finishTime as FinishTime from Attendance inner join Member on Attendance.memberID = Member.memberID where Attendance.date = @date) as Summary order by startTime asc";
            using (connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(query, connection))
            using (SqlDataAdapter adapter = new SqlDataAdapter(command))
            {
                command.Parameters.AddWithValue("@date", date);
                DataTable attendance = new DataTable();
                adapter.Fill(attendance);

                if (attendance.Rows.Count != 0)
                {
                    message = "Data succesfully received";
                    dataTable = attendance;  
                }

                else
                {
                    message = "No data found!";
                    dataTable = null;
                }
            }
        }

        private bool isAttendanceIDValid(int id)
        {
            connection = new SqlConnection(connectionString);
            string query = "select count(*) count from Attendance where attendanceID = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", id);

            try
            {
                connection.Open();
                int count = (Int32)command.ExecuteScalar();
                connection.Close();
                if (count != 0)
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }

            catch (SqlException)
            {
                return false;
            }
        }

        public bool searchAttendanceByID(string id, out string message)
        {
            int convertedID;
            if (int.TryParse(id, out convertedID))
            {
                if (isAttendanceIDValid(convertedID))
                {
                    string query = "select * from Attendance where attendanceID = @id";
                    using (connection = new SqlConnection(connectionString))
                    using (SqlCommand command = new SqlCommand(query, connection))
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        command.Parameters.AddWithValue("@id", convertedID);
                        DataTable attendance = new DataTable();
                        adapter.Fill(attendance);

                        this.attendanceID = convertedID;
                        this.startTime = Convert.ToDateTime(attendance.Rows[0]["startTime"].ToString());
                        this.finishTime = Convert.ToDateTime(attendance.Rows[0]["finishTime"].ToString());
                        this.date = Convert.ToDateTime(attendance.Rows[0]["date"]);
                        this.memberID = Convert.ToInt32(attendance.Rows[0]["memberID"]);
                        message = null;
                        return true;

                    }
                }

                else
                {
                    message = "ID entered does not exist in the database";
                    return false;
                }
            }
            else
            {
                message = "Invalid format of id";
                return false;
            }
        }

        
    }
}
