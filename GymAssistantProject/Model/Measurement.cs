﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymAssistantProject.Model
{
    public class Measurement
    {
        private SqlConnection connection;
        private string connectionString = ConfigurationManager.ConnectionStrings["GymAssistantProject.Properties.Settings.GymAssistantDBConnectionString"].ConnectionString;

        private int measureID;
        private int memberID;
        private DateTime date;
        private List<double> measurements;
        
        
        public bool setMemberID(string memberID, out string message)
        {
            try
            {
                int id = Convert.ToInt32(memberID);
                if (isMemberIDValid(id, out message))
                {
                    this.memberID = id;
                    return true;
                }

                else
                {
                    return false;
                }  
            }
            catch (FormatException)
            {
                message = "Member id format is not a valid integer";
                return false;
            }
        }

        public int getMemberID()
        {
            return memberID;
        }

        private bool isMemberIDValid(int id, out string messsage)
        {
            connection = new SqlConnection(connectionString);
            string query = "select count(*) count from Member where Member.memberID = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", id);

            try
            {
                connection.Open();
                int count = (Int32)command.ExecuteScalar();
                connection.Close();
                if (count != 0)
                {
                    messsage = null;
                    return true;
                }

                else
                {
                    messsage = "Member id does not exist in the database";
                    return false;
                }
            }

            catch (SqlException)
            {
                messsage = "Internal database error";
                return false;
            }
        }

        public void setDate(DateTime date)
        {
            this.date = date;
        }

        public DateTime getDate()
        {
            return date;
        }

        public bool setMeasurements(List<string> measurements, out string message)
        {
            this.measurements = new List<double>();
            for (int num = 0; num <= 14; ++num)
            {
                if (num <= 1)
                {
                    try
                    {
                        double value = double.Parse(measurements[num]);
                        if (value > 0 && value < 1000)
                        {
                            this.measurements.Add(value);
                        }
                        else
                        {
                            message = "One or many measurements are out of range";
                            return false;
                        }
                    }
                    catch (FormatException)
                    {
                        message = "One or many measurements are in invalid format or they may be empty";
                        return false;
                    }

                }

                else
                {
                    try
                    {
                        double value = double.Parse(measurements[num]);
                        if (value > 0 && value < 100)
                        {
                            this.measurements.Add(value);
                        }
                        else
                        {
                            message = "One or many measurements are out of range";
                            return false;
                        }
                    }
                    catch (FormatException)
                    {
                        message = "One or many measurements are in invalid format or they may be empty";
                        return false;
                    }
                }
            }
            message = null;
            return true;
        }

        public List<double> getMeasurements()
        {
            return measurements;
        }

        public bool addNewMeasurement(out string message)
        {
            connection = new SqlConnection(connectionString);

            string query = "insert into [Measurement] values (@memberID, @date, @weight, @height, @bmi, @fat, " +
            "@chest, @waist, @hip, @leftArm, @rightArm, @leftForearm, @rightForearm, @leftThigh, @rightThigh, @leftCalf, @rightCalf)";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@memberID", this.memberID);
            command.Parameters.AddWithValue("@date", this.date);
            command.Parameters.AddWithValue("@weight", this.measurements[0]);
            command.Parameters.AddWithValue("@height", this.measurements[1]);
            command.Parameters.AddWithValue("@bmi", this.measurements[2]);
            command.Parameters.AddWithValue("@fat", this.measurements[3]);
            command.Parameters.AddWithValue("@chest", this.measurements[4]);
            command.Parameters.AddWithValue("@waist", this.measurements[5]);
            command.Parameters.AddWithValue("@hip", this.measurements[6]);
            command.Parameters.AddWithValue("@leftArm", this.measurements[7]);
            command.Parameters.AddWithValue("@rightArm", this.measurements[8]);
            command.Parameters.AddWithValue("@leftForearm", this.measurements[9]);
            command.Parameters.AddWithValue("@rightForearm", this.measurements[10]);
            command.Parameters.AddWithValue("@leftThigh", this.measurements[11]);
            command.Parameters.AddWithValue("@rightThigh", this.measurements[12]);
            command.Parameters.AddWithValue("@leftCalf", this.measurements[13]);
            command.Parameters.AddWithValue("@rightCalf", this.measurements[14]);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                message = "Measurements have been successfully saved";
                return true;
            }
            catch (SqlException)
            {
                message = "Could not save the measuremetns due to database error";
                return false;
            }
        }

        public bool updateMeasurement(out string message)
        {
            connection = new SqlConnection(connectionString);

            string query = "update [Measurement] set memberID = @memberID, date = @date, weight = @weight, height = @height, bmi = @bmi, fat = @fat, " +
            "chest = @chest, waist = @waist, hip = @hip, leftArm = @leftArm, rightArm = @rightArm, leftForearm = @leftForearm, rightForearm = @rightForearm, leftThigh = @leftThigh, rightThigh = @rightThigh, leftCalf = @leftCalf, rightCalf = @rightCalf";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@memberID", this.memberID);
            command.Parameters.AddWithValue("@date", this.date);
            command.Parameters.AddWithValue("@weight", this.measurements[0]);
            command.Parameters.AddWithValue("@height", this.measurements[1]);
            command.Parameters.AddWithValue("@bmi", this.measurements[2]);
            command.Parameters.AddWithValue("@fat", this.measurements[3]);
            command.Parameters.AddWithValue("@chest", this.measurements[4]);
            command.Parameters.AddWithValue("@waist", this.measurements[5]);
            command.Parameters.AddWithValue("@hip", this.measurements[6]);
            command.Parameters.AddWithValue("@leftArm", this.measurements[7]);
            command.Parameters.AddWithValue("@rightArm", this.measurements[8]);
            command.Parameters.AddWithValue("@leftForearm", this.measurements[9]);
            command.Parameters.AddWithValue("@rightForearm", this.measurements[10]);
            command.Parameters.AddWithValue("@leftThigh", this.measurements[11]);
            command.Parameters.AddWithValue("@rightThigh", this.measurements[12]);
            command.Parameters.AddWithValue("@leftCalf", this.measurements[13]);
            command.Parameters.AddWithValue("@rightCalf", this.measurements[14]);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                message = "Measurements have been successfully updated";
                return true;
            }
            catch (SqlException)
            {
                message = "Could not update the measuremetns due to database error";
                return false;
            }
        }

        private bool isMeasurementIDValid(string id, out string message)
        {
            int convertedID;
            if (int.TryParse(id, out convertedID))
            {
                connection = new SqlConnection(connectionString);
                string query = "select count(*) from Measurement where Measurement.measureID = @id";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@id", convertedID);

                try
                {
                    connection.Open();
                    int count = (Int32)command.ExecuteScalar();
                    connection.Close();
                    if (count != 0)
                    {
                        this.measureID = convertedID;
                        message = null;
                        return true;
                    }

                    else
                    {
                        message = "Measurement id entered does not exist in the database";
                        return false;
                    }
                }

                catch (SqlException)
                {
                    message = "Cannot retrieve information due to database error";
                    return false;
                }
            }

            else
            {
                message = "Measurement id entered is not a valid integer";
                return false;
            }
        } 

        public bool retrieveMeasurementDetails(string id, out string message)
        {
            if (isMeasurementIDValid(id, out message))
            {
                string query = "select * from Measurement where measureID = @id";
                using (connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    command.Parameters.AddWithValue("@id", this.measureID);
                    DataTable info = new DataTable();
                    adapter.Fill(info);

                    if (info.Rows.Count != 0)
                    {
                        this.memberID = Convert.ToInt32(info.Rows[0]["memberID"]);
                        this.date = Convert.ToDateTime(info.Rows[0]["date"]);
                        measurements = new List<double>();
                        for (int num = 3; num <= 17; ++num)
                        {
                            this.measurements.Add(Convert.ToDouble(info.Rows[0][num]));
                        }
                        return true; 
                    }

                    else
                    {
                        message = "No records to show";
                        return false;
                    }
                }
            }

            else
            {
                return false;
            }
            
        }

        public bool deleteMeasurement(out string message)
        {
            connection = new SqlConnection(connectionString);

            string query = "delete from Measurement where measureID = @id";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", this.measureID);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                message = "Measurement record has been successfully deleted";
                return true;
            }
            catch (SqlException)
            {
                message = "Measurement record cannot be deleted due to a database error";
                return false;
            }
        }

        public DataTable getPreviousMeasurements(string memberID, out string message)
        {
            DataTable info = new DataTable();
            try
            {
                int convertedID = Convert.ToInt32(memberID);
                if (isMemberIDValid(convertedID, out message))
                {
                    string query = "select measureID as 'Measurement ID', memberID as 'Member ID', date as 'Date', " +
                                   "weight as 'Weight (kg)', height as 'Height (cm)', bmi  as BMI, fat as 'Fat (%)', " +
                                   "chest as 'Chest (in)', waist as 'Waist (in)', hip as 'Hip (in)', leftArm as 'Left Arm " +
                                   "(in)', rightArm as 'Right Arm (in)', leftForearm as 'Left Forearm (in)', rightForearm" +
                                   " as 'Right Forearm (in)', leftThigh as 'Left Thigh (in)', rightThigh as 'Right Thigh" +
                                   " (in)', leftCalf as 'Left Calf (in)', rightCalf as 'Right Calf (in)' from" +
                                   " (select * from Measurement where memberID = 1) as Summary order by date desc";
                    using (connection = new SqlConnection(connectionString))
                    using (SqlCommand command = new SqlCommand(query, connection))
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        command.Parameters.AddWithValue("@id", convertedID);
                        adapter.Fill(info);

                        if (info.Rows.Count != 0)
                        {
                            return info;
                        }

                        else
                        {
                            message = "No records to show";
                            info = null;
                            return info;
                        }
                    }
                }

                else
                {
                    info = null;
                    return info;
                }
            }

            catch (FormatException)
            {
                message = "Member id enetered is not a valid integer";
                info = null;
                return info;
            }

        }

        public bool getPercentDifference(string id1, string id2, out List<double> differences, out string message)
        {
            Measurement measurement1 = new Measurement();
            Measurement measurement2 = new Measurement();
            if(measurement1.retrieveMeasurementDetails(id1, out message))
            {
                if(measurement2.retrieveMeasurementDetails(id2, out message))
                {
                    differences = new List<double>();
                    for(int num = 0; num <= 14; ++num)
                    {
                        double value1 = measurement1.getMeasurements()[num];
                        double value2 = measurement2.getMeasurements()[num];
                        double percentDifference = Math.Round((value2 - value1) / value1 * 100, 1);
                        differences.Add(percentDifference);
                    }
                    return true;
                }

                else
                {
                    differences = null;
                    return false;
                }
            }

            else
            {
                differences = null;
                return false;
            }
            
            
        }


    }
}
