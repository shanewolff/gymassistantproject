﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymAssistantProject.Model
{
    public class Member
    {
        private SqlConnection connection;
        private string connectionString = ConfigurationManager.ConnectionStrings["GymAssistantProject.Properties.Settings.GymAssistantDBConnectionString"].ConnectionString;
        //private string connectionString = "Data Source=(LocalDB)/MSSQLLocalDB;AttachDbFilename=|DataDirectory|/GymAssistantDB.mdf;Integrated Security=True";
        private int memberID;
        private string name = null;
        private string nic;
        private string address = null;
        private string email;
        private int height;
        private DateTime dob = DateTime.Today;
        private string contact;
        private string primary_goal;
        private int fitness_level;
        private string diseases;
        private DateTime registration_date;

        public bool setName(string name)
        {
            if (name.Any(Char.IsLetter))
            {
                if (!name.Any(Char.IsDigit))
                {
                    this.name = name;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public bool setNIC(string nic)
        {
            char[] characters = nic.ToCharArray();
            if (characters.Length == 10)
            {
                if (Char.IsLetter(characters[9]))
                {
                    for (int index = 0; index <= 8; ++index)
                    {
                        if (characters[index] >= '0' && characters[index] <= '9')
                        {
                            continue;
                        }

                        else
                        {
                            return false;
                        }
                    }
                    this.nic = nic;
                    return true;
                }

                else
                {
                    return false;
                }
            }

            else
            {
                return false;
            }
        }

        public bool setAddress(string address)
        {
            if (address != "") { this.address = address; return true; }
            else { return false; }
        }

        public void setEmail(string email)
        {
            this.email = email;
        }

        public bool setHeight(string height)
        {
            try
            {
                int number = Convert.ToInt32(height);
                if (number >= 100 && number <= 220)
                {
                    this.height = number;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public bool setDOB(DateTime date)
        {
            if (date <= DateTime.Today)
            {
                dob = date;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool setContact(string contact)
        {
            char[] digits = contact.ToCharArray();
            if (digits.Length == 10)
            {
                foreach (char digit in digits)
                {
                    if (digit >= '0' && digit <= '9')
                    {
                        continue;
                    }
                    else
                    {
                        return false;
                    }
                }
                this.contact = contact;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool setPrimaryGoal(string primaryGoal)
        {
            if (primaryGoal != "") { this.primary_goal = primaryGoal; return true; }
            else { return false; }
        }

        public bool setFitnessLevel(string fitnessLevel)
        {
            if (fitnessLevel.Equals("Beginner"))
            {
                fitness_level = 1;
                return true;
            }
            else if (fitnessLevel.Equals("Some Experience"))
            {
                fitness_level = 2;
                return true;
            }

            else if (fitnessLevel.Equals("Advanced"))
            {
                fitness_level = 3;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void setDiseases(string diseases)
        {
            this.diseases = diseases;
        }

        public bool setRegistrationDate(DateTime date)
        {
            if (date <= DateTime.Today)
            {
                this.registration_date = date;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool saveMember()
        {
            connection = new SqlConnection(connectionString);

            string query = "insert into [Member] values (@name, @nic, @address, @email, @height, @dob, @contact, @primaryGoal, @fitnessLevel, @diseases, @registrationDate)";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@name", name);
            command.Parameters.AddWithValue("@nic", nic);
            command.Parameters.AddWithValue("@address", address);
            command.Parameters.AddWithValue("@email", email);
            command.Parameters.AddWithValue("@height", height);
            command.Parameters.AddWithValue("@dob", dob);
            command.Parameters.AddWithValue("@contact", contact);
            command.Parameters.AddWithValue("@primaryGoal", primary_goal);
            command.Parameters.AddWithValue("@fitnessLevel", fitness_level);
            command.Parameters.AddWithValue("@diseases", diseases);
            command.Parameters.AddWithValue("@registrationDate", registration_date);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                return true;

            }
            catch (SqlException)
            {
                return false;
            }


        }

        public DataTable getMembers()
        {
            string query = "select * from Member";
            using (connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(query, connection))
            using (SqlDataAdapter adapter = new SqlDataAdapter(command))
            {
                DataTable members = new DataTable();
                adapter.Fill(members);
                return members;
            }
        }

        public DataTable getMemberByName(string name)
        {
            string query = "select * from Member where Member.name = @name";
            using (connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(query, connection))
            using (SqlDataAdapter adapter = new SqlDataAdapter(command))
            {
                command.Parameters.AddWithValue("@name", name);
                DataTable members = new DataTable();
                adapter.Fill(members);
                if(members.Select().Length != 0)
                {
                    this.memberID = Convert.ToInt32(members.Rows[0]["memberID"]);
                    this.name = members.Rows[0]["name"].ToString();
                    this.nic = members.Rows[0]["nic"].ToString();
                    this.address = members.Rows[0]["address"].ToString();
                    this.email = members.Rows[0]["email"].ToString();
                    this.height = Convert.ToInt32(members.Rows[0]["height"]);
                    this.dob = Convert.ToDateTime(members.Rows[0]["dob"]);
                    this.contact = members.Rows[0]["contact"].ToString();
                    this.primary_goal = members.Rows[0]["primaryGoal"].ToString();
                    this.fitness_level = Convert.ToInt32(members.Rows[0]["fitnessLevel"]);
                    this.diseases = members.Rows[0]["diseases"].ToString();
                    this.registration_date = Convert.ToDateTime(members.Rows[0]["registrationDate"]);
                }
                return members;
            }
        }

        public DataTable getMemberByID(int id)
        {
            string query = "select * from Member where Member.memberID = @id";
            using (connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(query, connection))
            using (SqlDataAdapter adapter = new SqlDataAdapter(command))
            {
                command.Parameters.AddWithValue("@id", id);
                DataTable members = new DataTable();
                adapter.Fill(members);
                if (members.Select().Length != 0)
                {
                    this.memberID = Convert.ToInt32(members.Rows[0]["memberID"]);
                    this.name = members.Rows[0]["name"].ToString();
                    this.nic = members.Rows[0]["nic"].ToString();
                    this.address = members.Rows[0]["address"].ToString();
                    this.email = members.Rows[0]["email"].ToString();
                    this.height = Convert.ToInt32(members.Rows[0]["height"]);
                    this.dob = Convert.ToDateTime(members.Rows[0]["dob"]);
                    this.contact = members.Rows[0]["contact"].ToString();
                    this.primary_goal = members.Rows[0]["primaryGoal"].ToString();
                    this.fitness_level = Convert.ToInt32(members.Rows[0]["fitnessLevel"]);
                    this.diseases = members.Rows[0]["diseases"].ToString();
                    this.registration_date = Convert.ToDateTime(members.Rows[0]["registrationDate"]);
                }
                return members;
            }
        }

        public bool updateMember(int id)
        {
            connection = new SqlConnection(connectionString);
            string query = "update Member set name = @name, nic = @nic, address = @address, " +
                "email = @email, height = @height, dob = @dob, contact = @contact, primaryGoal = @primaryGoal, " +
                "fitnessLevel = @fitnessLevel, diseases = @diseases, registrationDate = @registrationDate where memberID = @id";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@name", name);
            command.Parameters.AddWithValue("@nic", nic);
            command.Parameters.AddWithValue("@address", address);
            command.Parameters.AddWithValue("@email", email);
            command.Parameters.AddWithValue("@height", height);
            command.Parameters.AddWithValue("@dob", dob);
            command.Parameters.AddWithValue("@contact", contact);
            command.Parameters.AddWithValue("@primaryGoal", primary_goal);
            command.Parameters.AddWithValue("@fitnessLevel", fitness_level);
            command.Parameters.AddWithValue("@diseases", diseases);
            command.Parameters.AddWithValue("@registrationDate", registration_date);
            command.Parameters.AddWithValue("@id", id);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                return true;

            }
            catch (SqlException)
            {
                return false;
            }
        }

        public bool saveToPDF(string path)
        {
            string fitness;
            if(this.fitness_level == 1)
            {
                fitness = "Beginner";
            }
            else if(this.fitness_level == 2)
            {
                fitness = "Some Experience";
            }
            else
            {
                fitness = "Advanced";
            }
            string n = Environment.NewLine;
            string content = "GymAsistant - MEMBER INFORMATION" + n +
                             "Member ID :" + this.memberID + n +
                             "Name :" + this.name + n +
                             "NIC :" + this.nic + n +
                             "Address :" + this.address + n +
                             "Email :" + this.email + n +
                             "Height :" + this.height + " cm" + n +
                             "Date of Birth :" + this.dob.ToShortDateString() + n +
                             "Contact :" + this.contact + n +
                             "Primary Goal :" + this.primary_goal + n +
                             "Fitness Level :" + fitness + n +
                             "Diseases :" + this.diseases + n +
                             "Registration Date :" + this.registration_date.ToShortDateString();

            if (PDFMaker.createPDF(path, content))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
