﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymAssistantProject.Model
{
    public class Subscription
    {
        private SqlConnection connection;
        private string connectionString = ConfigurationManager.ConnectionStrings["GymAssistantProject.Properties.Settings.GymAssistantDBConnectionString"].ConnectionString;

        private int subscriptionID;
        private int memberID;
        private string type;
        private DateTime commenceDate;
        private DateTime expiryDate;
        private string state;

        public bool setMemberID(int id)
        {
            if (isMemberIDValid(id))
            {
                this.memberID = id;
                return true;
            }

            else
            {
                return false;
            }
        }

        public int getMemberID()
        {
            return this.memberID;
        }

        public void setType(string type)
        {
            this.type = type;
        }

        public string getType()
        {
            return this.type;
        }

        public void setCommenceDate(DateTime date)
        {
            this.commenceDate = date;
        }

        public DateTime getCommenceDate()
        {
            return this.commenceDate;
        }

        public void setExpiryDate()
        {
            if (this.type.Equals("Month"))
            {
                this.expiryDate = this.commenceDate.AddMonths(1);
                setState();

            }

            if (this.type.Equals("Quater"))
            {
                this.expiryDate = this.commenceDate.AddMonths(3);
                setState();
            }

            if (this.type.Equals("Year"))
            {
                this.expiryDate = this.commenceDate.AddMonths(12);
                setState();
            }
        }

        public DateTime getExpiryDate()
        {
            return this.expiryDate;
        }

        public bool addSubscription()
        {
            connection = new SqlConnection(connectionString);

            string query = "insert into [Subscription] values (@type, @commenceDate, @expiryDate, @memberID)";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@type", this.type);
            command.Parameters.AddWithValue("@commenceDate", this.commenceDate);
            command.Parameters.AddWithValue("@expiryDate", this.expiryDate);
            command.Parameters.AddWithValue("@memberID", this.memberID);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }

        public string getDetails()
        {
            string info = "Member ID: " + Convert.ToString(memberID)
                        + Environment.NewLine + "Subscription Type: " + type
                        + Environment.NewLine + "Commence Date: " + commenceDate.ToShortDateString() + Environment.NewLine
                        + "Expiry Date: " + expiryDate.ToShortDateString() + Environment.NewLine + "Subscription State: " + this.state;
            return info;
        }

        public void setState()
        {
            if (expiryDate < DateTime.Today)
            {
                this.state = "Expired";
            }

            else
            {
                this.state = "Active";
            }
        }

        public string getState()
        {
            return state;
        }

        public void getSubscription(int memberID, out DataTable subscriptionInfo)
        {
            string query = "select * from (select subscriptionID as SubscriptionID, memberID as MemberID, type as Type, commenceDate as Commence, expiryDate as Expiry from Subscription where Subscription.memberID = @id) as table1 order by Expiry desc";
            using (connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(query, connection))
            using (SqlDataAdapter adapter = new SqlDataAdapter(command))
            {
                command.Parameters.AddWithValue("@id", memberID);
                DataTable info = new DataTable();
                adapter.Fill(info);

                if (info.Rows.Count != 0)
                {
                    subscriptionInfo = info;
                }

                else
                {
                    subscriptionInfo = null;
                }
            }
        }

        private bool isMemberIDValid(int id)
        {
            connection = new SqlConnection(connectionString);
            string query = "select count(*) count from Member where Member.memberID = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", id);

            try
            {
                connection.Open();
                int count = (Int32)command.ExecuteScalar();
                connection.Close();
                if (count != 0)
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }

            catch (SqlException)
            {
                return false;
            }
        }

        private bool isSubscriptionIDValid(int id)
        {
            connection = new SqlConnection(connectionString);
            string query = "select count(*) count from Subscription where Subscription.subscriptionID = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", id);

            try
            {
                connection.Open();
                int count = (Int32)command.ExecuteScalar();
                connection.Close();
                if (count != 0)
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }

            catch (SqlException)
            {
                return false;
            }
        }

        public bool searchSubscriptionByID(int id)
        {
            if (isSubscriptionIDValid(id))
            {
                string query = "select * from Subscription where Subscription.subscriptionID = @id";
                using (connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    command.Parameters.AddWithValue("@id", id);
                    DataTable subscription = new DataTable();
                    adapter.Fill(subscription);

                    this.subscriptionID = id;
                    this.type = subscription.Rows[0]["type"].ToString();
                    this.commenceDate = Convert.ToDateTime(subscription.Rows[0]["commenceDate"]);
                    setExpiryDate();
                    this.memberID = Convert.ToInt32(subscription.Rows[0]["memberID"]);
                    return true;

                }
            }

            else
            {
                return false;
            }
            
        }

        public bool updateSubscription()
        {
            connection = new SqlConnection(connectionString);

            string query = "update Subscription set type = @type, commenceDate = @commenceDate, expiryDate = @expiryDate, memberID = @memberID where Subscription.subscriptionID = @subscriptionID";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@type", this.type);
            command.Parameters.AddWithValue("@commenceDate", this.commenceDate);
            command.Parameters.AddWithValue("@expiryDate", this.expiryDate);
            command.Parameters.AddWithValue("@memberID", this.memberID);
            command.Parameters.AddWithValue("@subscriptionID", this.subscriptionID);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }

        public bool deleteSubscription()
        {
            connection = new SqlConnection(connectionString);

            string query = "delete from Subscription where subscriptionID = @id";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", this.subscriptionID);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (SqlException)
            {
                return false;
            }
        }

        public DataTable getExpiringList()
        {
            DataTable expiringList = new DataTable();

            string query = "select subscriptionID as SubscriptionID, memberID as MemberID, type as Type, commenceDate as Commence, expiryDate as Expiry from Subscription where Subscription.expiryDate >= @today and Subscription.expiryDate <= @futureDate";
            using (connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(query, connection))
            using (SqlDataAdapter adapter = new SqlDataAdapter(command))
            {
                command.Parameters.AddWithValue("@today", DateTime.Today);
                command.Parameters.AddWithValue("@futureDate", DateTime.Today.AddDays(7));
                adapter.Fill(expiringList);

                if (expiringList.Rows.Count != 0)
                {
                    return expiringList;
                }

                else
                {
                    expiringList = null;
                    return expiringList;
                }
            }

        }
    }
}
