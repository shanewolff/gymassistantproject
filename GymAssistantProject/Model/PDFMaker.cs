﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace GymAssistantProject.Model
{
    public static class PDFMaker
    {
        public static bool createPDF(string path, string content)
        {
            try
            {
                FileStream fs = new FileStream(path, FileMode.Create);
                Document document = new Document(PageSize.A4, 25, 25, 30, 30);
                PdfWriter writer = PdfWriter.GetInstance(document, fs);
                var font = FontFactory.GetFont("Courier", 18, BaseColor.BLACK);
                document.Open();
                var para = new Paragraph(content);
                para.Font = font;
                document.Add(para);
                document.Close();
                writer.Close();
                fs.Close();
                return true;
            }

            catch (Exception e)
            {
                return false;
            }
            
        }

    }
}
