﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Windows.Forms;

namespace GymAssistantProject.Model
{
    public class Users
    {
        private SqlConnection connection;
        private string connectionString = ConfigurationManager.ConnectionStrings["GymAssistantProject.Properties.Settings.GymAssistantDBConnectionString"].ConnectionString;
        //private string connectionString = "";
        private int userID;
        private string username;
        private string password;
        private string salt;
        private string type;

        public bool setUsername(string username, out string message)
        {
            if (username.Length != 0)
            {
                if (isUsernameUnique(username))
                {
                    this.username = username;
                    message = null;
                    return true;
                }

                else
                {
                    message = "The username already exists in the datatbase. Please choose another";
                    return false;
                }

            }
            else
            {
                message = "Username cannot be empty";
                return false;
            }

        }

        public string getUsername()
        {
            return this.username;
        }

        public bool setPassword(string password, out string message)
        {
            if (password.Length >= 8)
            {
                this.salt = createSalt(10);
                this.password = generateHash(password, salt);
                message = null;
                return true;
            }

            else
            {
                message = "Password length should be minimum 8 characters";
                return false;
            }

        }

        public void setType(string type)
        {
            this.type = type;
        }

        public string getType()
        {
            return this.type;
        }

        public bool addUser(out string message)
        {
            connection = new SqlConnection(connectionString);

            string query = "insert into [Users] values (@username, @password, @salt, @type)";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@username", this.username);
            command.Parameters.AddWithValue("@password", this.password);
            command.Parameters.AddWithValue("@salt", this.salt);
            command.Parameters.AddWithValue("@type", this.type);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                message = "User have been successfully saved";
                return true;
            }
            catch (SqlException)
            {
                message = "Could not save the user due to database error";
                return false;
            }
        }

        private bool changePassword(string userID, string oldPassword, string newPassword, out string message)
        {
            string message1;
            int id;
            if (isUserIDValid(userID, out message1, out id))
            {
                if (isOldPasswordValid(id, oldPassword))
                {
                    string salt = createSalt(10);
                    string new_password = generateHash(newPassword, createSalt(10));


                    connection = new SqlConnection(connectionString);

                    string query = "update [User] set password = @password, salt = @salt where userID = @userID";

                    SqlCommand command = new SqlCommand(query, connection);
                    command.Parameters.AddWithValue("@password", new_password);
                    command.Parameters.AddWithValue("@userID", id);
                    command.Parameters.AddWithValue("@salt", salt);

                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                        connection.Close();
                        message = "Password have been successfully updated";
                        return true;
                    }
                    catch (SqlException)
                    {
                        message = "Could not update the password due to database error";
                        return false;
                    }
                }

                else
                {
                    message = "Old password is incorrect";
                    return false;
                }
            }

            else
            {
                message = message1;
                return false;
            }
        }

        private bool isOldPasswordValid(int userID, string oldPassword)
        {
            connection = new SqlConnection(connectionString);
            string query = "select password, salt from User where User.userID = @id";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", userID);

            try
            {
                connection.Open();
                SqlDataReader dataReader = command.ExecuteReader();
                connection.Close();
                dataReader.Read();
                string password = dataReader["password"].ToString();
                salt = dataReader["salt"].ToString();
                dataReader.Close();

                if (generateHash(oldPassword, salt).Equals(password))
                {
                    return true;
                }

                else
                {
                    return false;
                }
            }

            catch (SqlException)
            {
                return false;
            }

        }

        private bool isUserIDValid(string id, out string message, out int userID)
        {
            int convertedID;
            if (int.TryParse(id, out convertedID))
            {
                connection = new SqlConnection(connectionString);
                string query = "select count(*) from Users where Users.userID = @id";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@id", convertedID);

                try
                {
                    connection.Open();
                    int count = (Int32)command.ExecuteScalar();
                    connection.Close();
                    if (count != 0)
                    {
                        this.userID = convertedID;
                        message = null;
                        userID = convertedID;
                        return true;
                    }

                    else
                    {
                        message = "User id entered does not exist in the database";
                        userID = 0;
                        return false;
                    }
                }

                catch (SqlException)
                {
                    message = "Cannot retrieve information due to database error";
                    userID = 0;
                    return false;
                }
            }

            else
            {
                message = "User id entered is not a valid integer";
                userID = 0;
                return false;
            }
        }

        private bool isUsernameUnique(string username)
        {
            connection = new SqlConnection(connectionString);
            string query = "select count(*) from Users where Users.username = @username";
            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@username", username);

            try
            {
                connection.Open();
                int count = (Int32)command.ExecuteScalar();
                connection.Close();
                if (count != 0)
                {
                    return false;
                }

                else
                {
                    return true;
                }
            }

            catch (SqlException)
            {
                return false;
            }
        }

        public DataTable getUsersInfo(out string message)
        {
            string query = "select userID as ID, username as Username, type as Category from Users";
            using (connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(query, connection))
            using (SqlDataAdapter adapter = new SqlDataAdapter(command))
            {
                DataTable info = new DataTable();
                adapter.Fill(info);

                if (info.Rows.Count != 0)
                {
                    message = null;
                    return info;
                }

                else
                {
                    message = "No records to display";
                    return null;
                }
            }
        }

        public bool getUser(string userID, out string status)
        {
            string message;
            int id;
            if (isUserIDValid(userID, out message, out id))
            {
                string query = "select userID as ID, username as Username, type as Category from Users where userID = @id";
                using (connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    command.Parameters.AddWithValue("@id", id);
                    DataTable info = new DataTable();
                    adapter.Fill(info);
                    this.userID = Convert.ToInt32(info.Rows[0]["ID"]);
                    this.username = info.Rows[0]["Username"].ToString();
                    this.password = null;
                    this.type = info.Rows[0]["Category"].ToString();
                    status = null;
                    return true;
                }
            }
            else
            {
                status = message;
                return false;
            }
        }

        private string createSalt(int size)
        {
            var rng = new RNGCryptoServiceProvider();
            var buff = new byte[size];
            rng.GetBytes(buff);
            return Convert.ToBase64String(buff);
        }

        private string generateHash(string password, string salt)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(password + salt);
            SHA256Managed hashString = new SHA256Managed();
            byte[] hash = hashString.ComputeHash(bytes);
            return Convert.ToBase64String(hash);
        }

        public bool deleteUser(out string message)
        {
            connection = new SqlConnection(connectionString);

            string query = "delete from Users where userID = @id";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", this.userID);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                message = "User is successfully deleted";
                return true;
            }
            catch (SqlException)
            {
                message = "User cannot be deleted due to database error";
                return false;
            }
        }

        public bool authenticate(string username, string password, out string type)
        {
            string query = "select * from Users where username = @username";
            using (connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(query, connection))
            using (SqlDataAdapter adapter = new SqlDataAdapter(command))
            {
                command.Parameters.AddWithValue("@username", username);
                DataTable info = new DataTable();
                adapter.Fill(info);
                if (info.Rows.Count != 0)
                {
                    string salt = info.Rows[0]["salt"].ToString();
                    string passwordEntered = generateHash(password, salt);
                    string passwordExisting = info.Rows[0]["password"].ToString();

                    if (passwordEntered.Equals(passwordExisting))
                    {
                        type = info.Rows[0]["type"].ToString();
                        return true;
                    }

                    else
                    {
                        type = null;
                        return false;
                    }
                }

                else
                {
                    type = null;
                    return false;
                }
            }
        }
    }
}
