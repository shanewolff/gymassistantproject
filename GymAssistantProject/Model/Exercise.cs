﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GymAssistantProject.Model
{
    public class Exercise
    {
        private SqlConnection connection;
        private string connectionString = ConfigurationManager.ConnectionStrings["GymAssistantProject.Properties.Settings.GymAssistantDBConnectionString"].ConnectionString;

        int exerciseID;
        string name;
        string type;

        public bool setName(string name, out string message)
        {
            if(name.Length != 0)
            {
                this.name = name;
                message = null;
                return true;
            }
            else
            {
                message = "Name is empty. The name cannot be empty";
                return false;
            }
            
        }

        public string getName()
        {
            return this.name;
        }

        public bool setType(string type, out string message)
        {
            if (type.Length != 0)
            {
                this.type = type;
                message = null;
                return true;
            }
            else
            {
                message = "Type is empty. The type cannot be empty";
                return false;
            }
        }

        public string getType()
        {
            return this.type;
        }

        public bool addExercise(out string message)
        {
            connection = new SqlConnection(connectionString);

            string query = "insert into [Exercise] values (@name, @type)";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@name", this.name);
            command.Parameters.AddWithValue("@type", this.type);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                message = "The exercise details are successfully saved";
                return true;
            }
            catch (SqlException)
            {
                message = "The exercise could not be saved due to a database error";
                return false;
            }
        }

        public void getExercises(out DataTable exerciseInfo)
        {
            string query = "select exerciseID as ID, name as Name, type as Type from Exercise";
            using (connection = new SqlConnection(connectionString))
            using (SqlCommand command = new SqlCommand(query, connection))
            using (SqlDataAdapter adapter = new SqlDataAdapter(command))
            {
                DataTable info = new DataTable();
                adapter.Fill(info);

                if (info.Rows.Count != 0)
                {
                    exerciseInfo = info;
                }

                else
                {
                    exerciseInfo = null;
                }
            }
        }

        public bool getExerciseByID(string id, out string message, out int error)
        {
            try
            {
                int convertedID = Convert.ToInt32(id);

                string query = "select * from Exercise where Exercise.exerciseID = @id";
                using (connection = new SqlConnection(connectionString))
                using (SqlCommand command = new SqlCommand(query, connection))
                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    command.Parameters.AddWithValue("@id", convertedID);
                    DataTable info = new DataTable();
                    adapter.Fill(info);

                    if (info.Rows.Count != 0)
                    {
                        this.exerciseID = Convert.ToInt32(info.Rows[0]["exerciseID"]);
                        this.name = info.Rows[0]["name"].ToString();
                        this.type = info.Rows[0]["type"].ToString();
                        message = null;
                        error = 0;
                        return true;
                    }

                    else
                    {
                        message = "No record exists for the given id number";
                        error = 1;
                        return false;
                    }
                }

            }

            catch (FormatException)
            {
                message = "Invalid format of id. Only integers are valid";
                error = 2;
                return false;
            }

            
        }

        public bool deleteExercise(out string message)
        {
            connection = new SqlConnection(connectionString);

            string query = "delete from Exercise where exerciseID = @id";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@id", this.exerciseID);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                message = "Record is successfully deleted";
                return true;
            }
            catch (SqlException)
            {
                message = "Record cannot be deleted due to database error";
                return false;
            }
        }

        public bool updateExercise(out string message)
        {
            connection = new SqlConnection(connectionString);

            string query = "update Exercise set name = @name, type = @type where Exercise.exerciseID = @exerciseID";

            SqlCommand command = new SqlCommand(query, connection);
            command.Parameters.AddWithValue("@type", this.type);
            command.Parameters.AddWithValue("@name", this.name);
            command.Parameters.AddWithValue("@exerciseID", this.exerciseID);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                message = "Record is successfully updated";
                return true;
            }
            catch (SqlException)
            {
                message = "Record cannot be updated due to database error";
                return false;
            }
        }
    }
}
