﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using GymAssistantProject.View;

namespace GymAssistantProject
{
    public static class Program
    {
        public static LoginForm loginInstance;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            loginInstance = new LoginForm();
            Application.Run(loginInstance);

        }
    }
}
